QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});


QUnit.test('Testing with Palindrome', function (assert) {

    assert.equal(Palindrome(10), "The given number 10 is Not a Palindrome number", 'Tested with 10 and gives output as not palindrome ');
    assert.equal(Palindrome(121), "The given number 121 is a Palindrome number", 'Tested with 121 and gives output as palindrome');
    assert.equal(Palindrome(-1), "The given number -1 is Not a Palindrome number", 'Tested with -1 and gives output as not palindrome ');
    assert.equal(Palindrome(11), "The given number 11 is a Palindrome number", 'Tested with 11 and gives output as palindrome ');
    
}
);

// QUnit.test('Fibonacci()', function (assert) {
//     assert.equal(Fibonacci(9), 55, 'tested with positive number');
//     assert.throws(function () { (Fibonacci(-1)); }, /you must choose between >=1/,'The given number is not a positive number');
// });

